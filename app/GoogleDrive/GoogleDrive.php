<?php

namespace App\GoogleDrive;

use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Illuminate\Http\UploadedFile;

class GoogleDrive
{
    protected $drive;

    public function __construct()
    {
        // $client = new Google_Client();
        // $client->setAuthConfig(storage_path('client_secret.json'));
        // $client->setAccessType('offline');


        $client = new Google_Client();
        $client->setClientId(env('GOOGLE_CLIENT_ID'));
        $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $client->setRedirectUri(env('GOOGLE_REDIRECT_URI'));
        $accessToken = '1//048Pr6OzObG8qCgYIARAAGAQSNwF-L9Irf2qKgXl9Gw0fX_jKqftPvVMD8cirzd9m2WZh11bhqH0kh-L-EtgUeWReXWztfQy9-is';
        $client->setAccessToken($accessToken);

        $this->drive = new Google_Service_Drive($client);
    }

    public function upload($file)
    {
        $fileMetadata = new Google_Service_Drive_DriveFile([
            'name' => $file->getClientOriginalName(),
        ]);

        $uploadedFile = $this->drive->files->create($fileMetadata, [
            'data' => file_get_contents($file->getRealPath()),
            'uploadType' => 'multipart',
            'fields' => 'id',
        ]);

        return $uploadedFile->id;
    }
}