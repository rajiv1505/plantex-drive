@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Product') }}</div>

                <div class="card-body">
                    <form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">
                	@csrf
                      <div class="mb-3">
                        <label for="productName" class="form-label">Product Name</label>
                        <input type="text" name="product_name" class="form-control" id="productName">
                      </div>
                      <div class="mb-3">
                        <label for="ProductSku" class="form-label">Product SKU</label>
                        <input type="text" name="sku" class="form-control" id="ProductSku">
                      </div>
                      <div class="mb-3">
                        <label for="productImage" class="form-label">Product Images</label>
                        <input type="file" name="images[]" class="form-control" id="productImage" multiple>
                      </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
